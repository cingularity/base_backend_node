import mongoose from "mongoose";

const graveSchema = new mongoose.Schema(
  {
    graveNumber: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    latitude: {
      type: String,
    },
    longitude: {
      type: String,
    },
    length: { type: String },
    breadth: { type: String },
    // deceaseds: [{ type: mongoose.Schema.Types.ObjectId, ref: "Deceased" }],
    // graveOwnerId: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "GraveOwner",
    // },
    // gravetypeId: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "Gravetype",
    // },
    graveyardId: { type: mongoose.Schema.Types.ObjectId, ref: "Graveyard" },
  },
  {
    timestamps: true,
  }
);

const Grave = mongoose.model("Grave", graveSchema);
export default Grave;
