import mongoose from "mongoose";
import { GENDER_ENUM } from "../enum";

const graveyardSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    licenseCount: {
      type: String,
      required: true,
      default: "1",
    },
    // address: { type: mongoose.Schema.Types.ObjectId, ref: "Address" },
    shortName: { type: String, required: true, unique: true },
    // graves: [{ type: mongoose.Schema.Types.ObjectId, ref: "Grave" }],
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    // licenseCount: { type: Number, required: true, default: 3 },
    // salutations: [{ type: String, enum: GENDER_ENUM }],
    // titles: [{ type: String }],
    // paymentMethods: [
    //   { type: mongoose.Schema.Types.ObjectId, ref: "PaymentMethod" },
    // ],
  },
  {
    timestamps: true,
  }
);

const Graveyard = mongoose.model("Graveyard", graveyardSchema);
export default Graveyard;
