import mongoose from "mongoose";

const documentationSchema = new mongoose.Schema(
  {
    date: {
      type: Date,
      default: Date.now,
    },
    information: {
      type: String,
    },
    graveId: { type: mongoose.Schema.Types.ObjectId, ref: "Grave" },
    // images: [{ type: mongoose.Schema.Types.ObjectId, ref: "Image" }],
  },
  {
    timestamps: true,
  }
);

const Documentation = mongoose.model("Documentation", documentationSchema);
export default Documentation;
