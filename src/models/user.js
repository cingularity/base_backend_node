import mongoose from "mongoose";
import bcrypt from "bcrypt";
import isEmail from "validator/lib/isEmail";
import { GENDER_ENUM, LANGAUGES_ENUM, ROLES_ENUM } from "../enum";

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
      validate: [isEmail, "No valid email address provided."],
    },
    password: {
      type: String,
      required: true,
      minlength: 7,
      maxlength: 42,
    },
    role: {
      type: String,
      enum: ROLES_ENUM,
      default: ROLES_ENUM.USER,
    },
    employeeId: {
      type: String,
    },
    firstName: {
      type: String,
    },
    middleName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    profileImage: {
      type: String,
    },
    sex: {
      type: String,
      enum: GENDER_ENUM,
    },
    confirmed: {
      type: Boolean,
      default: false,
    },
    activated: {
      type: Boolean,
      default: false,
    },
    language: {
      type: String,
      enum: LANGAUGES_ENUM,
      default: LANGAUGES_ENUM.DE,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.statics.findByLogin = async function (login) {
  let user = await this.findOne({
    username: login,
  });

  if (!user) {
    user = await this.findOne({ email: login });
  }

  return user;
};

userSchema.pre("remove", function (next) {
  this.model("Message").deleteMany({ userId: this._id }, next);
});

userSchema.pre("save", async function () {
  this.password = await this.generatePasswordHash();
});

userSchema.methods.generatePasswordHash = async function () {
  const saltRounds = 10;
  return await bcrypt.hash(this.password, saltRounds);
};

userSchema.methods.validatePassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

const User = mongoose.model("User", userSchema);

export default User;
