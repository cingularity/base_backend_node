import mongoose from "mongoose";

import User from "./user";
import Message from "./message";
import Graveyard from "./graveyard";
// import Group from './group';
import Address from "./address";
// import Landline from './landline';
// import Mobile from './mobile';
// import Email from './email';
// import Web from './web';
// import Bank from './bank';
// import Image from './image';
import Grave from "./grave";
// import GraveOwner from './graveOwner';
import Gravetype from './gravetype';
import Coffintype from './coffintype';
// import Deceased from './deceased';
import Fee from './fee';
import Documentation from "./documentation";
// import Salutation from './salutation';
// import Title from './title';
// import PaymentMethod from "./paymentMethod";
// import CalendarEvent from './calendarEvent';
// import Token from './token';

import {
  ENV,
  DATABASE_URL,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  TEST_DATABASE_URL,
  TEST_DATABASE_USERNAME,
  TEST_DATABASE_PASSWORD,
} from "../config";

const connectDb = () => {
  if (ENV === "PRODUCTION") {
    return mongoose.connect(DATABASE_URL, {
      useNewUrlParser: true,
      auth: {
        user: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
      },
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
  }

  if (ENV === "TEST") {
    // console.log('IN TEST ENVIRONMENT');
    return mongoose.connect(TEST_DATABASE_URL, {
      useNewUrlParser: true,
      auth: {
        user: TEST_DATABASE_USERNAME,
        password: TEST_DATABASE_PASSWORD,
      },
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
  }

  if (ENV === "DEVELOPMENT") {
    return mongoose.connect(DATABASE_URL, {
      useNewUrlParser: true,
      auth: {
        user: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
      },
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
  }

  console.log(
    "Please set an environment NODE_ENV= either DEVELOPMENT, PRODUCTION or TEST"
  );
};

const models = {
  User,
  Message,
  Graveyard,
  // Group,
  // Address,
  // Landline,
  // Mobile,
  // Email,
  // Web,
  // Bank,
  // Image,
  Grave,
  // GraveOwner,
  Gravetype,
  Coffintype,
  // Deceased,
  Fee,
  Documentation,
  // Salutation,
  // Title,
  // PaymentMethod,
  // CalendarEvent,
  // Token,
};

export { connectDb };

export default models;
