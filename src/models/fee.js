import mongoose from "mongoose";
import { PERIOD_ENUM } from "../enum";

const feeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    price: {
      type: String,
      required: true,
    },
    period: {
      type: String,
      enum: PERIOD_ENUM,
      default: PERIOD_ENUM.YEARLY,
    },
    graveyardId: { type: mongoose.Schema.Types.ObjectId, ref: "Graveyard" },
  },
  { timestamps: true }
);

const Fee = mongoose.model("Fee", feeSchema);
export default Fee;
