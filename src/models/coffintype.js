import mongoose from "mongoose";

const coffintypeSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String },
    material: { type: String },
    graveyardId: { type: mongoose.Schema.Types.ObjectId, ref: "Graveyard" },
  },
  { timestamps: true }
);

const Coffintype = mongoose.model("Coffintype", coffintypeSchema);
export default Coffintype;
