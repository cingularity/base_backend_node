import mongoose from "mongoose";
import { GRAVETYPE_PERIOD_ENUM } from "../enum";

const gravetypeSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String },
    color: { type: String },
    cost: { type: String },
    period: {
      type: String,
      enum: GRAVETYPE_PERIOD_ENUM,
      default: GRAVETYPE_PERIOD_ENUM.YEARLY,
    },
    graveyardId: { type: mongoose.Schema.Types.ObjectId, ref: "Graveyard" },
  },
  { timestamps: true }
);

const Gravetype = mongoose.model("Gravetype", gravetypeSchema);
export default Gravetype;
