import { PubSub } from "apollo-server";

import * as MESSAGE_EVENTS from "./message";
import * as FEE_EVENTS from "./fee";
import * as COFFINTYPE_EVENTS from "./coffintype";
import * as GRAVETYPE_EVENTS from "./gravetype";
import * as GRAVE_EVENTS from "./grave";
import * as DOCUMENTATION_EVENTS from './documentation';
// import * as CALENDAR_EVENTS from './calendar';
// import * as GRAVEYARD_EVENTS from './graveyard';

export const EVENTS = {
  MESSAGE: MESSAGE_EVENTS,
  FEE: FEE_EVENTS,
  COFFINTYPE: COFFINTYPE_EVENTS,
  GRAVETYPE: GRAVETYPE_EVENTS,
  GRAVE: GRAVE_EVENTS,
  DOCUMENTATION: DOCUMENTATION_EVENTS
  // CALENDAR: CALENDAR_EVENTS,
  // GRAVEYARD: GRAVEYARD_EVENTS,
};

export default new PubSub();

///////////----not working ------------/////////

// // import { RedisPubSub } from 'graphql-redis-subscriptions';
// // import Redis from 'ioredis';

// // import * as MESSAGE_EVENTS from './message';

// // export const EVENTS = {
// //   MESSAGE: MESSAGE_EVENTS,
// // };

// // const options = {
// //   host: '127.0.0.1',
// //   port: 6379,
// //   retryStrategy: (times) => {
// //     return Math.min(times * 50, 2000);
// //   },
// // };

// // const dataReviver = (key, value) => {
// //   const isISO8601Z = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/;
// //   if (typeof value === 'string' && isISO8601Z.test(value)) {
// //     const tempDateNumber = Date.parse(value);
// //     if (!isNaN(tempDateNumber)) {
// //       return new Date(tempDateNumber);
// //     }
// //   }
// //   return value;
// // };

// // // export default new RedisPubSub();

// // const redis_client = new Redis(options);
// // const redis_subscriber = new Redis(options);

// // const pubsub = new RedisPubSub({
// //   publisher: redis_client,
// //   subscriber: redis_subscriber,
// //   reviver: dataReviver,
// // });

// // export default pubsub;

// import { RedisPubSub } from "graphql-redis-subscriptions";
// import Redis from "ioredis";

// import * as MESSAGE_EVENTS from "./message";
// import * as FEE_EVENTS from "./fee";
// import * as COFFINTYPE_EVENTS from "./coffintype";
// import * as GRAVETYPE_EVENTS from "./gravetype";
// import * as GRAVE_EVENTS from "./grave";

// export const EVENTS = {
//   MESSAGE: MESSAGE_EVENTS,
//   FEE: FEE_EVENTS,
//   COFFINTYPE: COFFINTYPE_EVENTS,
//   GRAVETYPE: GRAVETYPE_EVENTS,
//   GRAVE: GRAVE_EVENTS,
//   // CALENDAR: CALENDAR_EVENTS,
//   // GRAVEYARD: GRAVEYARD_EVENTS,
// };

// const options = {
//   host: "127.0.0.1",
//   port: 6379,
//   retryStrategy: (times) => {
//     return Math.min(times * 50, 2000);
//   },
// };

// const redis_client = new Redis(options);
// const redis_subscriber = new Redis(options);

// const pubsub = new RedisPubSub({
//   publisher: redis_client,
//   subscriber: redis_subscriber,
//   // reviver: dataReviver,
// });

// export default pubsub;
