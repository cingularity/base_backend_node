import jwt from "jsonwebtoken";
import { combineResolvers } from "graphql-resolvers";
import { AuthenticationError, UserInputError } from "apollo-server";
import { sendEmail } from "../mailer/email";
import { isAdmin, isAuthenticated, isSuperAdmin } from "./authorization";

const createToken = async (user, secret, expiresIn = 3600) => {
  const { id, email, username, role } = user;
  return await jwt.sign({ id, email, username, role }, secret, {
    expiresIn,
  });
};

export default {
  Query: {
    users: combineResolvers(
      isAuthenticated,
      isSuperAdmin,
      async (parent, args, { models }) => {
        return await models.User.find();
      }
    ),
    user: combineResolvers(
      isAuthenticated,
      isAdmin,
      async (parent, { id }, { models }) => {
        return await models.User.findById(id);
      }
    ),
    me: async (parent, args, { models, me }) => {
      if (!me) {
        return null;
      }
      return await models.User.findById(me.id);
    },
  },
  Mutation: {
    signUp: combineResolvers(async (parent, { input }, { models, secret }) => {
      let user = await models.User.findByLogin(input.username);
      if (!user) {
        user = new models.User({ ...input });
        user.save();
      } else {
        return user;
      }
      const token = await models.Token.create({
        _userId: user._id,
        token: await createToken(user, secret, 2800),
      });
      sendEmail
        .send({
          template: "friedhof",
          message: {
            to: input.email,
          },
          locals: {
            name: `${input.firstName} ${input.lastName}`,
            site: process.env.SITE_URL,
            link: `${process.env.SITE_URL}/new-user/${token.token}`,
          },
        })
        .then(console.log)
        .catch(console.error);
      return user;
    }),
    confirmUser: combineResolvers(async (parent, { token }, { models }) => {
      const result = await models.Token.findOne({ token });
      if (!result) return { status: false };
      const filter = { _id: result._userId };
      const update = { confirmed: true };
      const opts = { new: true };
      const doc = await models.User.findOneAndUpdate(filter, update, opts);
      if (!doc.confirmed) return { status: false };
      await result.remove();
      return { status: true };
    }),
    signIn: async (parent, { login, password }, { models, secret }) => {
      const user = await models.User.findByLogin(login);

      if (!user)
        throw new UserInputError("No user found with this login credentials.");

      const isValid = await user.validatePassword(password);

      if (!isValid) {
        throw new AuthenticationError("Invalid password.");
      }

      return {
        token: createToken(user, secret, parseInt(process.env.TOKEN_AGE, 10)),
        expires: `${process.env.TOKEN_AGE}`,
        role: user.role,
      };
    },

    updateUser: combineResolvers(
      isAuthenticated,
      async (parent, { input }, { models, me }) => {
        return await models.User.findByIdAndUpdate(
          me.id,
          { ...input },
          { new: true }
        );
      }
    ),

    updateUserRole: combineResolvers(
      isAuthenticated,
      isAdmin,
      async (parent, { id, role }, { models }) => {
        return await models.User.findByIdAndUpdate(id, { role }, { new: true });
      }
    ),
    toggleUserActivation: combineResolvers(
      isAuthenticated,
      isAdmin,
      async (parent, { id }, { models }) => {
        const user = await models.User.findById(id);
        if (!user) throw new UserInputError("user does not exist");
        await user.updateOne({ activated: !user.activated });
        return { status: true };
      }
    ),
    deleteUser: combineResolvers(
      isAuthenticated,
      isAdmin,
      async (parent, { id }, { models }) => {
        const user = await models.User.findById(id);

        if (user) {
          await user.remove();
          return { status: true };
        } else {
          return { status: false };
        }
      }
    ),
  },
  User: {
    messages: async (user, args, { models }) => {
      return await models.Message.find({
        userId: user.id,
      });
    },
    graveyards: async (user, args, { models, me }) =>
      await models.Graveyard.find({ users: me.id }),

    // graveyards: async (user, args, { models, me }) => {
    //   const graveyards = (await models.Graveyard.find()).filter((graveyard) =>
    //     graveyard.users.includes(user.id)
    //   );
    //   return graveyards;
    // },
  },
};
