import { UserInputError } from "apollo-server-errors";
import { combineResolvers } from "graphql-resolvers";
import { withFilter } from "graphql-subscriptions";
import pubsub, { EVENTS } from "../subscription";
import { isAuthenticated } from "./authorization";

export default {
  Query: {
    graves: combineResolvers(
      isAuthenticated,
      async (parent, args, { models }) => await models.Grave.find()
    ),
    grave: combineResolvers(
      isAuthenticated,
      async (parent, { id }, { models }) => await models.Grave.findById(id)
    ),
  },
  Mutation: {
    updateGrave: combineResolvers(
      isAuthenticated,
      async (parent, { id, input }, { models }) => {
        const grave = await models.Grave.findById(id);
        if (!grave) throw new UserInputError("_grave_does_not_exist");
        grave.set({ ...input });
        grave.save();
        pubsub.publish(EVENTS.GRAVE.UPDATED, {
          graveUpdated: grave,
        });
        return grave;
      }
    ),
    addDocumentation: combineResolvers(
      isAuthenticated,
      async (parent, { id, input }, { models }) => {
        const grave = await models.Grave.findById(id);
        if (!grave) throw new UserInputError("_grave_does_not_exist");
        const documentation = await new models.Documentation({
          ...input,
          graveId: id,
        }).save();

        pubsub.publish(EVENTS.DOCUMENTATION.CREATED, {
          documentCreated: { graveId: id, documentation },
        });
        return grave;
      }
    ),
  },
  Grave: {
    documentations: combineResolvers(
      isAuthenticated,
      async (grave, args, { models }) =>
        await models.Documentation.find({ graveId: grave.id })
    ),
  },
  Subscription: {
    graveUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.GRAVE.UPDATED),
        (payload, variables) => variables.id === payload.graveUpdated.id
      ),
    },
    documentCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.DOCUMENTATION.CREATED),
        (payload, variables) =>
          payload.documentCreated.graveId === variables.graveId
      ),
    },
  },
};
