import { GraphQLDateTime } from "graphql-iso-date";

import userResolvers from "./user";
import messageResolvers from "./message";
import graveyardResolvers from "./graveyard";
// import groupResolvers from './group';
// import addressResolvers from './address';
// import landlineResolvers from './landline';
// import mobileResolvers from './mobile';
// import emailResolvers from './email';
// import webResolvers from './web';
// import bankResolvers from './bank';
// import imageResolvers from './image';
import graveResolvers from "./grave";
// import graveOwnerResolvers from './graveOwner';
// import gravetypeResolvers from './gravetype';
// import bodyContainerResolvers from './bodyContainer';
// import deceasedResolvers from './deceased';
// import reportResolvers from './report';
// import feeResolvers from './fee';
// import documentationResolvers from './documentation';
// import calendarEventResolves from './calendarEvent';
import coffintypeResolvers from "./coffintype";

const customScalarResolver = {
  Date: GraphQLDateTime,
};

export default [
  customScalarResolver,
  userResolvers,
  messageResolvers,
  graveyardResolvers,
  // groupResolvers,
  // addressResolvers,
  // landlineResolvers,
  // mobileResolvers,
  // emailResolvers,
  // webResolvers,
  // bankResolvers,
  // imageResolvers,
  graveResolvers,
  // graveOwnerResolvers,
  // gravetypeResolvers,
  // bodyContainerResolvers,
  // deceasedResolvers,
  // reportResolvers,
  // feeResolvers,
  // documentationResolvers,
  // calendarEventResolves,
  coffintypeResolvers,
];
