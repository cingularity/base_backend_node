import { UserInputError } from "apollo-server-errors";
import { combineResolvers } from "graphql-resolvers";
import { withFilter } from "graphql-subscriptions";
import pubsub, { EVENTS } from "../subscription";
import { isAuthenticated } from "./authorization";

export default {
  Query: {
    graveyard: combineResolvers(
      isAuthenticated,
      async (parent, { id }, { models }) => await models.Graveyard.findById(id)
    ),
    graveyards: combineResolvers(
      isAuthenticated,
      async (parent, args, { models, me }) =>
        await models.Graveyard.find({ users: me.id })
    ),
  },
  Mutation: {
    /////////////// ADD NEW ////////////////
    newGraveyard: combineResolvers(
      isAuthenticated,
      async (parent, { input }, { models, me }) => {
        const { name } = input;
        const _graveyard = await models.Graveyard.findOne({ name });
        if (!_graveyard) {
          const _new_graveyard = new models.Graveyard({
            ...input,
            users: [me.id],
          }).save();
          return _new_graveyard;
        }
        return _graveyard;
      }
    ),
    addFeeToGraveyard: combineResolvers(
      isAuthenticated,
      async (parent, { graveyardId, input }, { models }) => {
        const graveyard = await models.Graveyard.findById(graveyardId);
        if (!graveyard) throw new UserInputError("_graveyard_does_not_exist");
        const fee = await new models.Fee({ ...input, graveyardId }).save();
        pubsub.publish(EVENTS.FEE.CREATED, {
          feeCreated: { graveyardId, fee },
        });
        return graveyard;
      }
    ),
    addCoffintypeToGraveyard: combineResolvers(
      isAuthenticated,
      async (parent, { graveyardId, input }, { models }) => {
        const graveyard = await models.Graveyard.findById(graveyardId);
        if (!graveyard) throw new UserInputError("_graveyard_does_not_exist");
        const coffintype = await new models.Coffintype({
          ...input,
          graveyardId,
        }).save();
        pubsub.publish(EVENTS.COFFINTYPE.CREATED, {
          coffintypeCreated: { graveyardId, coffintype },
        });
        return graveyard;
      }
    ),
    addGravetypeToGraveyard: combineResolvers(
      isAuthenticated,
      async (parent, { graveyardId, input }, { models }) => {
        const graveyard = await models.Graveyard.findById(graveyardId);
        if (!graveyard) throw new UserInputError("_graveyard_does_not_exist");
        const gravetype = await new models.Gravetype({
          ...input,
          graveyardId,
        }).save();
        pubsub.publish(EVENTS.GRAVETYPE.CREATED, {
          gravetypeCreated: { graveyardId, gravetype },
        });
        return graveyard;
      }
    ),
    addGraveToGraveyard: combineResolvers(
      isAuthenticated,
      async (parent, { graveyardId, input }, { models }) => {
        const graveyard = await models.Graveyard.findById(graveyardId);
        if (!graveyard) throw new UserInputError("_graveyard_does_not_exist");
        const grave = await new models.Grave({ ...input, graveyardId }).save();
        pubsub.publish(EVENTS.GRAVE.CREATED, {
          graveCreated: { graveyardId, grave },
        });
        return graveyard;
      }
    ),
    ///////////////////////////////////
    ///////////// UPDATE //////////////
    // updateGraveOnGraveyard: combineResolvers(
    //   isAuthenticated,
    //   async (parent, { graveyardId, input }, { models }) => {
    //     const graveyard = await models.Graveyard.findById(graveyardId);
    //     if (!graveyard) throw new UserInputError("_graveyard_does_not_exist");
    //     const grave = await models.Grave.findById(input.id);
    //     grave.set({ ...input, graveyardId });
    //     grave.save();
    //     // graveyard.save();
    //     pubsub.publish(EVENTS.GRAVE.UPDATED, {
    //       graveUpdated: { graveyardId, grave },
    //     });
    //     return graveyard;
    //   }
    // ),
    ///////////////////////////////////
  },
  Graveyard: {
    users: combineResolvers(
      isAuthenticated,
      async (graveyard, args, { loaders, me }) =>
        await graveyard.users.map((user) => loaders.user.load(user))
    ),
    gravetypes: combineResolvers(
      isAuthenticated,
      async (graveyard, args, { models }) =>
        await models.Gravetype.find({ graveyardId: graveyard.id })
    ),
    coffintypes: combineResolvers(
      isAuthenticated,
      async (graveyard, args, { models }) =>
        await models.Coffintype.find({ graveyardId: graveyard.id })
    ),
    graves: combineResolvers(
      isAuthenticated,
      async (graveyard, args, { models }) =>
        await models.Grave.find({ graveyardId: graveyard.id })
    ),
    fees: combineResolvers(
      isAuthenticated,
      async (graveyard, args, { models }) =>
        await models.Fee.find({ graveyardId: graveyard.id })
    ),
  },
  Subscription: {
    feeCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.FEE.CREATED),
        (payload, variables) =>
          variables.graveyardId === payload.feeCreated.graveyardId
      ),
    },
    coffintypeCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.COFFINTYPE.CREATED),
        (payload, variables) =>
          variables.graveyardId === payload.coffintypeCreated.graveyardId
      ),
    },
    gravetypeCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.GRAVETYPE.CREATED),
        (payload, variables) =>
          variables.graveyardId === payload.gravetypeCreated.graveyardId
      ),
    },
    graveCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.GRAVE.CREATED),
        (payload, variables) =>
          variables.graveyardId === payload.graveCreated.graveyardId
      ),
    },
  },
};
