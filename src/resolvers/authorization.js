import { ForbiddenError } from "apollo-server";
import { skip } from "graphql-resolvers";
import { ROLES_ENUM } from "../config";

export const isSuperAdmin = (parent, args, { me }) => {
  if (me.role !== ROLES_ENUM.SUPERADMIN) throw new ForbiddenError("Forbidden");
  skip;
};

export const isAdmin = (parent, args, { me }) => {
  if (me.role !== ROLES_ENUM.ADMIN)
    throw new ForbiddenError("Admin privileges required.");
  skip;
};

export const isModerator = (parent, args, { me }) => {
  if (me.role !== ROLES_ENUM.MODERATOR)
    throw new ForbiddenError(
      "Elevated privileges required. Contact your system administrator"
    );
  skip;
};

export const isAuthenticated = (parent, args, { me }) =>
  me ? skip : new ForbiddenError("Not authenticated as user.");

export const isActivated = (parent, args, { me }) =>
  me.isActivated ? skip : new ForbiddenError("Not authenticated as user.");

export const isMessageOwner = async (parent, { id }, { models, me }) => {
  const message = await models.Message.findById(id);
  if (message.userId != me.id) {
    throw new ForbiddenError("Not authenticated as owner.");
  }

  return skip;
};

export const isGraveyardUser = async (parent, { id }, { models, me }) => {
  const graveyard = await models.Graveyard.findById(id);
  if (!graveyard.users.includes(me.id)) {
    throw new ForbiddenError("Not authenticated as graveyard user");
  }
  return skip;
};
