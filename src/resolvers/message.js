import { combineResolvers } from 'graphql-resolvers';
import { withFilter } from 'apollo-server';
import pubsub, { EVENTS } from '../subscription';
import {
  isAdmin,
  isAuthenticated,
  isMessageOwner,
} from './authorization';

const toCursorHash = (string) =>
  Buffer.from(string).toString('base64');

const fromCursorHash = (string) =>
  Buffer.from(string, 'base64').toString('ascii');

export default {
  Query: {
    messages: async (parent, { cursor, limit = 100 }, { models }) => {
      const cursorOptions = cursor
        ? {
            createdAt: {
              $lt: fromCursorHash(cursor),
            },
          }
        : {};
      const messages = await models.Message.find(
        cursorOptions,
        null,
        {
          sort: { createdAt: -1 },
          limit: limit + 1,
        },
      );

      const hasNextPage = messages.length > limit;
      const edges = hasNextPage ? messages.slice(0, -1) : messages;

      return {
        edges,
        pageInfo: {
          hasNextPage,
          endCursor: toCursorHash(
            edges[edges.length - 1].createdAt.toString(),
          ),
        },
      };
    },

    message: combineResolvers(
      isAuthenticated,
      async (parent, { id }, { models }) => {
        return await models.Message.findById(id);
      },
    ),
    // graveyardMessages: combineResolvers(
    //   isAuthenticated,
    //   async (
    //     parent,
    //     { graveyardId, cursor, limit = 20 },
    //     { models },
    //   ) => {
    //     const cursorOptions = cursor
    //       ? {
    //           createdAt: {
    //             $lt: fromCursorHash(cursor),
    //           },
    //         }
    //       : {};
    //     const messages = await models.Message.find({
    //       graveyardId,
    //     }).find(cursorOptions, null, {
    //       sort: { createdAt: -1 },
    //       limit: limit + 1,
    //     });
    //     // return await models.Message.find({ graveyardId });
    //     const hasNextPage = messages.length > limit;
    //     const edges = hasNextPage ? messages.slice(0, -1) : messages;

    //     return {
    //       edges,
    //       pageInfo: {
    //         hasNextPage,
    //         endCursor: toCursorHash(
    //           edges[edges.length - 1].createdAt.toString(),
    //         ),
    //       },
    //     };
    //   },
    // ),
  },

  Mutation: {
    createMessage: combineResolvers(
      isAuthenticated,
      async (parent, { input }, { models, me }) => {
        const message = await models.Message.create({
          ...input,
          userId: me.id,
        });
        pubsub.publish(EVENTS.MESSAGE.CREATED, {
          messageCreated: { message },
        });

        return message;
      },
    ),

    deleteMessage: combineResolvers(
      isAuthenticated,
      isMessageOwner,
      async (parent, { id }, { models }) => {
        const message = await models.Message.findById(id);
        if (message) {
          await message.remove();
          return true;
        } else {
          return false;
        }
      },
    ),
  },

  Message: {
    user: async (message, args, { loaders }) => {
      return await loaders.user.load(message.userId);
    },
  },

  // Subscription: {
  //   messageCreated: {
  //     subscribe: withFilter(
  //       () => pubsub.asyncIterator(EVENTS.MESSAGE.CREATED),
  //       (payload, variables) =>
  //         variables.graveyardIds
  //           .map((item) => item.toString())
  //           .includes(
  //             payload.messageCreated.message.graveyardId.toString(),
  //           ),
  //     ),
  //     // will update the data before send
  //     // resolve: (payload)=> ({
  //     //   id:  payload.newEmployee.id,
  //     //   name: payload.newEmployee.name,
  //     //   employerId: payload.newEmployee.employerId
  //     // })
  //   },
  // },
};
