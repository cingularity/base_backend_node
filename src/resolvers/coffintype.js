import { UserInputError } from "apollo-server-errors";
import { combineResolvers } from "graphql-resolvers";
import { withFilter } from "graphql-subscriptions";
import pubsub, { EVENTS } from "../subscription";
import { isAuthenticated } from "./authorization";

export default {
  Query: {
    coffintypes: combineResolvers(
      isAuthenticated,
      async (parent, args, { models }) => await models.Coffintype.find()
    ),
    coffintype: combineResolvers(
      isAuthenticated,
      async (parent, { id }, { models }) => await models.Coffintype.findById(id)
    ),
  },
  Mutation: {
    updateCoffintype: combineResolvers(
      isAuthenticated,
      async (parent, { id, input }, { models }) => {
        const coffintype = await models.Coffintype.findById(id);
        if (!coffintype) throw new UserInputError("_coffintype_does_not_exist");
        coffintype.set({ ...input });
        coffintype.save();
        pubsub.publish(EVENTS.COFFINTYPE.UPDATED, {
          coffintypeUpdated: coffintype,
        });
        return coffintype;
      }
    ),
  },
  Coffintype: {},
  Subscription: {
    coffintypeUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(EVENTS.COFFINTYPE.UPDATED),
        (payload, variables) => variables.id === payload.coffintypeUpdated.id
      ),
    },
  },
};
