import models from './models';

export const initializeDB = async () => {
  const user0 = new models.User({
    username: 'admin',
    email: 'admin@ashah.de',
    password: 'admin123',
    role: 'SUPERADMIN',
  });

  const user1 = new models.User({
    username: 'amit',
    email: 'amit@ashah.de',
    password: 'amit123',
    role: 'ADMIN',
  });

  const user2 = new models.User({
    username: 'nikky',
    email: 'nikky@ashah.de',
    password: 'nikky123',
  });
  const user3 = new models.User({
    username: 'mylifenp@yahoo.com',
    email: 'mylifenp@yahoo.com',
    password: 'Dragon123',
    role: 'ADMIN',
  });

  user0.save();
  user1.save();
  user2.save();
  user3.save();

  // create 2 groups
  // const group1 = new models.Group({
  //   name: 'First Group',
  //   users: [user1.id, user2.id, user3.id],
  // });

  // const graveyard1 = new models.Graveyard({
  //   name: 'First Graveyard',
  //   users: [user1.id, user2.id],
  // });
  // const graveyard2 = new models.Graveyard({
  //   name: 'Second Graveyard',
  //   users: [user1.id, user3.id],
  // });

  // await graveyard1.save();
  // await graveyard2.save();
  // await group1.save();
};
