import { createTransport } from "nodemailer";
import Email from "email-templates";

import {
  EMAIL_SERVER,
  EMAIL_SMTP_PORT,
  EMAIL_USERNAME,
  EMAIL_PASSWORD,
} from "../config";

const transport = {
  host: EMAIL_SERVER,
  port: EMAIL_SMTP_PORT,
  secure: false,
  auth: {
    user: EMAIL_USERNAME,
    pass: EMAIL_PASSWORD,
  },
};

// export const transporter = createTransport(transport);
// transporter.verify((error, success) => {
//   if (error) {
//     console.error(error);
//   } else {
//     console.log('read to send mail');
//   }
// });

export const sendEmail = new Email({
  message: {
    from: EMAIL_USERNAME,
  },
  send: true,
  transport: createTransport(transport),
});
