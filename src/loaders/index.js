import * as user from './user';
import * as deceased from './deceased';
import * as gravetype from './gravetype';

export default { user, deceased, gravetype };
