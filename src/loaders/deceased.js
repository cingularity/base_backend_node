export const batchDeceased = async (keys, models) => {
  const deceaseds = await models.Deceased.find({
    _id: {
      $in: keys,
    },
  });

  return keys.map(key => deceaseds.find(deceased => deceased.id == key));
};
