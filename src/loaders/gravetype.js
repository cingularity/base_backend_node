export const batchGravetype = async (keys, models) => {
  console.log('in Gravetype')
  const gravetypes = await models.Gravetype.find({
    _id: {
      $in: keys,
    },
  });

  return keys.map(key => gravetypes.find(gravetype => gravetype.id == key));
};
