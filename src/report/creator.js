const PDFDocument = require('pdfkit');
import fs from 'fs';

export const createPdf = (text) => {
  let doc = new PDFDocument({ margin: 50 });
  doc
    .addPage()
    .fillColor('blue')
    .text(text, 100, 100)
    .underline(100, 100, 160, 27, { color: '#0000FF' })
    .link(100, 100, 160, 27, 'http://google.com/');
  doc.end();
  doc.pipe(fs.createWriteStream('output.pdf'));
};
