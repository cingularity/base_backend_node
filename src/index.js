import "dotenv/config";
// import fs from 'fs';
import { createStream } from "rotating-file-stream";
import path from "path";
// require('dotenv').config;
import cors from "cors";
import morgan from "morgan";
import http from "http";
import jwt from "jsonwebtoken";
import DataLoader from "dataloader";
import express from "express";
import { ApolloServer, AuthenticationError } from "apollo-server-express";

import schema from "./schema";
import resolvers from "./resolvers";
import models, { connectDb } from "./models";
import loaders from "./loaders";
import { initializeDB } from "./initializer";

import { PORT, ENV, INIT, SECRET } from "./config";

// const rfs = require('rotating-file-stream');

const app = express();
// const accessLogStream = rfs.createStream('access.log', {
const accessLogStream = createStream("access.log", {
  interval: "1d",
  path: path.join(__dirname, "log"),
});
app.use(cors());

app.use(
  morgan("dev", {
    skip: (req, res) => res.statusCode < 400,
  })
);

// logging all request to access.log
app.use(
  morgan("combined", {
    stream: accessLogStream,
  })
);

const validateToken = (token) => {
  return jwt.verify(token, SECRET);
};

const getMe = async (req) => {
  const token = req.headers["token"];
  if (token) {
    try {
      // return await jwt.verify(token, process.env.SECRET);
      return validateToken(token);
    } catch (e) {
      throw new AuthenticationError("Your session expired. Sign in again.");
    }
  }
};

// const myPlugin = {
//   // Fires whenever a GraphQL request is received from a client.
//   requestDidStart(requestContext) {
//     console.log(
//       'Request started! Query:\n' + requestContext.request.query,
//     );

//     return {
//       // Fires whenever Apollo Server will parse a GraphQL
//       // request to create its associated document AST.
//       parsingDidStart(requestContext) {
//         console.log('Parsing started!');
//       },

//       // Fires whenever Apollo Server will validate a
//       // request's document AST against your GraphQL schema.
//       validationDidStart(requestContext) {
//         console.log('Validation started!');
//       },
//     };
//   },
// };

const server = new ApolloServer({
  introspection: true,
  typeDefs: schema,
  resolvers,
  // plugins: [myPlugin],
  engine: {
    reportSchema: false,
  },
  formatError: (error) => {
    // remove the internal sequelize error message
    // leave only the important validation error
    const message = error.message
      .replace("SequelizeValidationError: ", "")
      .replace("Validation error: ", "");

    return {
      ...error,
      message,
    };
  },
  context: async ({ req, connection }) => {
    if (connection) {
      return {
        models,
        loaders: {
          user: new DataLoader((keys) => loaders.user.batchUsers(keys, models)),
        },
      };
    }

    if (req) {
      const me = await getMe(req);
      return {
        models,
        me,
        secret: SECRET,
        loaders: {
          user: new DataLoader((keys) => loaders.user.batchUsers(keys, models)),
        },
      };
    }
  },
  subscriptions: {
    onConnect: async (connectionParams, webSocket) => {
      if (connectionParams.token) {
        const { token } = connectionParams;
        try {
          if (validateToken(token)) {
            return {
              models,
              loaders: {
                user: new DataLoader((keys) =>
                  loaders.user.batchUsers(keys, models)
                ),
              },
            };
          }
        } catch (e) {
          throw new AuthenticationError("Your session expired. Sign in again.");
        }
        return { me };
      }
      throw new Error("Missing authentication token!");
    },
  },
});

server.applyMiddleware({ app, path: "/graphql" });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

connectDb().then(async () => {
  if (ENV === "TEST" || INIT) {
    try {
      await Promise.all([
        models.User.deleteMany({}),
        // models.Graveyard.deleteMany({}),
        // models.Group.deleteMany({}),
      ]);
    } catch (e) {
      console.log("could not delete all");
    }
    // reset database

    initializeDB();
  }

  httpServer.listen({ port: PORT }, () => {
    console.log(
      `Apollo Server on http://localhost:${PORT}${server.graphqlPath}`
    );
  });
});

// clean up before termination

process.on("SIGINT", function onSigint() {
  console.log("terminating the service onSigint");
  app.shutdown();
});

process.on("SIGTERM", function onSigterm() {
  console.log("terminating the service onSigterm");
  app.shutdown();
});

app.shutdown = function () {
  // clean up your resources and exit
  console.log("cleaning up before shutdown");
  process.exit();
};
