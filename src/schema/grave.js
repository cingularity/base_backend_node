import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    graves: [Grave]
    grave(id: ID!): Grave
  }
  extend type Mutation {
    # createGrave(input: GraveInput!): Grave
    updateGrave(id: ID!, input: GraveInput): Grave
    addDocumentation(id: ID!, input: DocumentationInput): Grave
    # deleteGrave(id: ID!): GraveDeleted
    # addDeceasedToGrave(id: ID!, deceasedId: ID!): Grave
    # removeDeceasedFromGrave(id: ID!, deceasedId: ID!): DeceasedDeleted
    # addDocumentationToGrave(id: ID!, input: DocumentationInput): Grave
    # removeDocumentationFromGrave(
    #   id: ID!
    #   documentationId: ID!
    # ): DocumentationDeleted
  }
  extend type Subscription {
    graveCreated(graveyardId: ID!): GraveCreated
    graveUpdated(id: ID!): Grave
  }

  # type GraveDeleted {
  #   status: Boolean!
  # }

  type Grave {
    id: ID!
    graveNumber: String
    description: String
    latitude: String
    longitude: String
    length: String
    breadth: String
    # graveOwner: GraveOwner
    # gravetype: Gravetype
    # deceaseds: [Deceased]
    documentations: [Documentation]
    updatedAt: Date
  }
  input GraveInput {
    graveNumber: String
    description: String
    latitude: String
    longitude: String
    length: String
    breadth: String
    # gravetype: ID
    # graveOwner: GraveOwnerInput
    # deceaseds: [DeceasedInput]
    # documentations: [DocumentationInput]
  }
  type GraveCreated {
    graveyardId: ID!
    grave: Grave
  }
`;
