import { gql } from "apollo-server-express";

export default gql`
  extend type Subscription {
    gravetypeCreated(graveyardId: ID!): GravetypeCreated
  }
  input GravetypeInput {
    name: String
    description: String
    color: String
    cost: String
    period: GravetypePeriod
  }
  type Gravetype {
    id: ID!
    name: String!
    description: String
    cost: String
    color: String
    period: GravetypePeriod
  }
  type GravetypeCreated {
    graveyardId: ID
    gravetype: Gravetype
  }
  enum GravetypePeriod {
    YEARLY
    MAX_REST_PERIOD
  }
`;
