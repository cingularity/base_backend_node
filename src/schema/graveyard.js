import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    graveyard(id: ID!): Graveyard!
    graveyards: [Graveyard!]
  }
  extend type Mutation {
    newGraveyard(input: GraveyardInput): Graveyard
    addFeeToGraveyard(graveyardId: ID!, input: FeeInput): Graveyard
    addCoffintypeToGraveyard(
      graveyardId: ID!
      input: CoffintypeInput
    ): Graveyard
    addGravetypeToGraveyard(graveyardId: ID!, input: GravetypeInput): Graveyard
    addGraveToGraveyard(graveyardId: ID!, input: GraveInput): Graveyard
    # updateGraveOnGraveyard(graveyardId: ID!, input: GraveInput): Graveyard
  }
  type Graveyard {
    id: ID!
    name: String!
    licenseCount: String
    shortName: String
    users: [User]
    gravetypes: [Gravetype]
    coffintypes: [Coffintype]
    graves: [Grave]
    fees: [Fee]
  }
  input GraveyardInput {
    name: String
    shortName: String
  }
`;
