import { gql } from "apollo-server-express";

export default gql`
  extend type Subscription {
    documentCreated(graveId: ID!): DocumentCreated
  }
  type Documentation {
    id: ID!
    date: Date
    information: String
  }

  input DocumentationInput {
    date: Date
    information: String
  }
  type DocumentCreated {
    graveId: ID!
    documentation: Documentation
  }
`;
