import { gql } from "apollo-server-express";

export default gql`
  # extend type Query {
  #   allFees: [Fee]
  #   fee(id: ID!): Fee
  # }
  # extend type Mutation {
  #   createFee(input: FeeInput!): Fee
  #   updateFee(id: ID!, input: FeeInput): Fee
  #   deleteFee(id: ID!): FeeRemoved
  # }

  extend type Subscription {
    feeCreated(graveyardId: ID!): FeeCreated
  }

  type FeeRemoved {
    status: Boolean!
  }

  type Fee {
    id: ID!
    name: String!
    description: String
    price: String
    period: FeePeriod
  }
  input FeeInput {
    name: String
    description: String
    price: String
    period: FeePeriod
  }
  type FeeCreated {
    graveyardId: ID!
    fee: Fee
  }
  enum FeePeriod {
    ONETIME
    MONTHLY
    YEARLY
  }
`;
