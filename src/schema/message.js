import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    messages(cursor: String, limit: Int): MessageConnection!
    message(id: ID!): Message!
    # graveyardMessages(
    #   graveyardId: ID!
    #   cursor: String
    #   limit: Int
    # ): MessageConnection!
  }

  extend type Mutation {
    createMessage(input: MessageInput): Message!
    deleteMessage(id: ID!): DeleteMessageResult!
  }

  # extend type Subscription {
  #   messageCreated(graveyardIds: [ID]!): MessageCreated!
  # }

  type DeleteMessageResult {
    status: Boolean!
  }

  type MessageConnection {
    edges: [Message!]!
    pageInfo: PageInfo!
  }

  type PageInfo {
    hasNextPage: Boolean!
    endCursor: String!
  }

  type Message {
    id: ID!
    text: String!
    createdAt: Date!
    user: User!
  }

  type MessageCreated {
    message: Message!
  }

  input MessageInput {
    id: ID!
    # graveyardId: ID!
    userId: ID!
    text: String!
  }
`;
