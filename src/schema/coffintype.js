import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    coffintypes: [Coffintype]
    coffintype(id: ID!): Coffintype
  }
  extend type Mutation {
    updateCoffintype(id: ID!, input: CoffintypeInput): Coffintype
  }
  extend type Subscription {
    coffintypeCreated(graveyardId: ID!): CoffintypeCreated
    coffintypeUpdated(id: ID!): Coffintype
  }
  type Coffintype {
    id: ID!
    name: String!
    description: String
    material: String
  }
  input CoffintypeInput {
    name: String!
    description: String
    material: String
  }
  type CoffintypeCreated {
    graveyardId: ID!
    coffintype: Coffintype
  }
`;
