import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    users: [User!]
    user(id: ID!): User
    me: User
  }

  extend type Mutation {
    signUp(input: SignUpUser!): User!
    signIn(login: String!, password: String!): Token!
    confirmUser(token: String!): ConfirmUserResult!
    updateUser(input: UserInput): User!
    deleteUser(id: ID!): DeleteUserResult!
    updateUserRole(id: ID!, role: String!): User!
    toggleUserActivation(id: ID!): UserActivationResult
  }

  type Token {
    token: String!
    expires: String!
    role: String
  }

  type DeleteUserResult {
    status: Boolean!
  }
  type ConfirmUserResult {
    status: Boolean!
  }
  type UserActivationResult {
    status: Boolean!
  }

  input AdminUserInput {
    username: String!
    email: String!
    password: String!
  }

  input SignUpUser {
    username: String!
    email: String!
    firstName: String
    lastName: String
    password: String!
  }

  type User {
    id: ID!
    username: String!
    email: String!
    role: String
    employeeId: String
    firstName: String
    middleName: String
    lastName: String
    profileImage: String
    sex: String
    activated: Boolean
    messages: [Message!]
    graveyards: [Graveyard!]
    # groups: [Group]
  }

  input UserInput {
    id: ID!
    username: String
    email: String
    employeeId: String
    firstName: String
    middleName: String
    lastName: String
    profileImage: String
    sex: String
    activated: Boolean
    role: String
  }
`;
