import { gql } from 'apollo-server-express';

import userSchema from './user';
import messageSchema from './message';
import graveyardSchema from './graveyard';
// import groupSchema from './group';
// import addressSchema from './address';
// import landlineSchema from './landline';
// import mobileSchema from './mobile';
// import emailSchema from './email';
// import webSchema from './web';
// import bankSchema from './bank';
// import imageSchema from './image';
import graveSchema from './grave';
// import graveOwnerSchema from './graveOwner';
import gravetypeSchema from './gravetype';
import coffintypeSchema from './coffintype';
// import deceasedSchema from './deceased';
// import reportSchema from './report';
import feeSchema from './fee';
import documentationSchema from './documentation';
// import salutationSchema from './salutation';
// import titleSchema from './title';
// import paymentTypeSchema from './paymentType';
// import calendarEventSchema from './calendarEvent';

const linkSchema = gql`
  scalar Date

  type Query {
    _: Boolean
  }

  type Mutation {
    _: Boolean
  }

  type Subscription {
    _: Boolean
  }
`;

export default [
  linkSchema,
  userSchema,
  messageSchema,
  graveyardSchema,
  // groupSchema,
  // addressSchema,
  // landlineSchema,
  // mobileSchema,
  // emailSchema,
  // webSchema,
  // bankSchema,
  // imageSchema,
  graveSchema,
  // graveOwnerSchema,
  gravetypeSchema,
  coffintypeSchema,
  // deceasedSchema,
  // reportSchema,
  feeSchema,
  documentationSchema,
  // salutationSchema,
  // titleSchema,
  // paymentTypeSchema,
  // calendarEventSchema,
];
