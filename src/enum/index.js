export const PERIOD_ENUM = {
  ONETIME: "ONETIME",
  MONTHLY: "MONTHLY",
  YEARLY: "YEARLY",
};

export const GRAVETYPE_PERIOD_ENUM = {
  YEARLY: "YEARLY",
  MAX_REST_PERIOD: "MAX_REST_PERIOD",
};
export const ROLES_ENUM = {
  USER: "USER",
  MODERATOR: "MODERATOR",
  ADMIN: "ADMIN",
  SUPERADMIN: "SUPERADMIN",
};
export const GENDER_ENUM = {
  MALE: "MALE",
  FEMALE: "FEMALE",
  DIVERSE: "DIVERSE",
};

export const LANGAUGES_ENUM = {
  EN: "English",
  DE: "German",
};
